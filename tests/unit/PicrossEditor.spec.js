// eslint-disable-next-line no-unused-vars
import { mount, shallowMount } from '@vue/test-utils'
// eslint-disable-next-line no-unused-vars
import PicrossEditor from '../../src/components/PicrossEditor.vue'

describe('PicrossEditor.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(PicrossEditor)
    expect(wrapper).toBeDefined()
  })
  it('renders with props', () => {
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows: 10,
        numberCells: 10
      }
    })
    expect(wrapper.vm.numberRows).toBe(10)
    expect(wrapper.vm.numberCells).toBe(10)
    expect(wrapper.vm.cellsValues).toBeDefined()
    expect(wrapper.vm.cellsValues.length).toBe(10)
    const cellsValuesExpected = []
    for (let i = 0; i < 10; i++) {
      const currentRowCellsValues = new Array(10).fill(false)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
    expect(wrapper).toMatchSnapshot()
  })
  it('emits update-picross-data', async () => {
    const onUpdatePicrossData = jest.fn()
    const wrapper = shallowMount(PicrossEditor, {
      propsData: { numberRows: 10, numberCells: 10 },
      listeners: {
        'update-picross-data': onUpdatePicrossData
      }
    })
    expect(wrapper).toBeDefined()
    const cell = wrapper.find('table tr:nth-child(2) td:nth-child(3)')
    await cell.trigger('click')
    expect(wrapper).toMatchSnapshot()
    expect(onUpdatePicrossData).toBeCalled()
    expect(onUpdatePicrossData.nock.calls.length).toBe()
  })
})
